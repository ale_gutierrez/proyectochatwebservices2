package com.example.agutierrez.proyectochatwebservices;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MiChat extends AppCompatActivity {

    private Button ingresarChat;// funcion GET
    private  Button enviarChat; // funcion POST
    private TableLayout tabla;
    private TableRow fila;
    TableRow.LayoutParams layoutFila;

    //esta es mi variable para enviar mensaje
    private String nuevo_msj;
    private String nuevo_idDest;

    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_chat);

        context = this;
        // aqui se gestiona el envio y recibo del chat
        ingresarChat = (Button)findViewById(R.id.btnIngresarChat);

        enviarChat = (Button)findViewById(R.id.btnEnviarMsj);

        tabla = ( TableLayout)findViewById(R.id.tabla);

        layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT);

        ingresarChat.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                tabla.removeAllViews();
                MetodoGet();
            }
        });

        enviarChat.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
                EditText txtMsj = (EditText)findViewById(R.id.txtMensaje);
                nuevo_msj = txtMsj.getText().toString();

                EditText idDest = (EditText)findViewById(R.id.idDestino);
                nuevo_idDest = idDest.getText().toString();

                MetodoPost();
            }
        });


    }

    private void MetodoPost() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Cargando...");
        pDialog.show();

        StringRequest jsArrayRequest = new StringRequest(
                Request.Method.POST,
                link_post,
                new Response.Listener<String>(){

                    @Override
                    public void onResponse(String response) {
                        if (pDialog != null)
                            if(pDialog.isShowing())
                                pDialog.hide();
                        try {
                            Log.e("Respuesta", response);
                            Resultado resultadoJson = new Gson().fromJson(response, Resultado.class);
                            if (resultadoJson.getCorrecto()==1){
                                Toast.makeText(getApplicationContext(),"Mensaje Enviado", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(getApplicationContext(), "Error de conexion", Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception ex){
                            Log.e("Ejemplo", "Respuesta Volley : " + ex.toString());
                        }
                    }
                },
                new Response.ErrorListener(){

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (pDialog!=null)
                            if (pDialog.isShowing())
                                pDialog.hide();
                        //manejo de errores
                        Log.e("Ejemplo", "Respuesta Volley : " + error.getMessage());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> pars = new HashMap<String, String>();
                pars.put("Content-Type", "application/x-www-form-urlencoded");
                return pars;
            }

            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> parametros = new HashMap();
                parametros.put("mensaje", nuevo_msj );
                parametros.put("destino", nuevo_idDest);
                //parametros.put("sueldo", nuevo_sueldo);
                return parametros;
            }
        };
        WsPeticiones.getInstance(context).addToRequestQueue(jsArrayRequest);

    }

    private ProgressDialog pDialog;
    private static String link_get = "http://dreamyourapps.com/webservices/chat/recibir.php?id=4";
    private static String link_post = "http://dreamyourapps.com/webservices/chat/enviar.php";

    private void MetodoGet() {

        pDialog = new ProgressDialog(context);

        HashMap<String, String> parametros = new HashMap();

        pDialog.setMessage("Cargando...");

        pDialog.show();

        JsonObjectRequest jsArrayRequest = new JsonObjectRequest(
                Request.Method.GET,
                link_get,
                new JSONObject(parametros),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (pDialog != null)
                            if (pDialog.isShowing())
                                pDialog.hide();
                        try {
                            Resultado resultadoJson = new Gson().fromJson(response.toString(), Resultado.class);

                            if (resultadoJson.getCorrecto() == 1) {
                                for (Chat chat : resultadoJson.getChat()) {

                                    //agregarFilas(chat.getMensaje(),String.valueOf(chat.getMensaje()));
                                    // aqui le anado mi id de destino
                                    agregarFilas(chat.getMensaje(), "4");
                                }
                                Toast.makeText(getApplicationContext(), "Ingreso al chat...", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Error de conexion...", Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception ex) {
                            Log.e("Ejemplo", "Respuesta Volley : " + ex.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(pDialog != null)
                            if(pDialog.isShowing())
                                pDialog.hide();
                        // manejo de errores
                        Log.e("Ejemplo", "Error : " +error.getMessage());
                    }
                }
        );
        WsPeticiones.getInstance(context).addToRequestQueue(jsArrayRequest);
    }

    private void agregarFilas(String mensaje, String s) {

        fila = new TableRow(this);

        fila.setLayoutParams(layoutFila);

        TextView msj = new TextView(this);
        TextView id = new TextView(this);

        msj.setText(mensaje);
        id.setText(s);

        msj.setBackgroundResource(R.drawable.celda_cuerpo);
  //      id.setBackgroundResource(R.drawable.celda_cuerpo);

        //agregamos las filas
        fila.addView(msj);
//        fila.addView(id);

        tabla.addView(fila);
    }

}
