package com.example.agutierrez.proyectochatwebservices;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

/**
 * Created by agutierrez on 25/04/2016.
 */
//public class RegistroUsuario extends AppCompatActivity implements Validator.ValidationListener, AdapterView.OnItemSelectedListener {
public class RegistroUsuario extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty(message = "Escriba su nombre",trim = true)
    private EditText nombreReg;

    @Password(min = 6, message = "Password 6 caracteres")
    private  EditText passReg;

    @ConfirmPassword(message = "Debe ser el mismo pass")
    private  EditText passReg2;

   /* @NotEmpty(message = "Seleccione una opcion!!!")
    private Spinner miSpinner;
    private String[] state = {"Seleccione una opcion", "Varoncito", "Mujercita"};
*/
    private Button regUsuario;

    Context context;

    Validator validator;

    int ban=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.registro);

        context = this;

        nombreReg = (EditText)findViewById(R.id.nombreReg);

        passReg = (EditText)findViewById(R.id.passReg);

        passReg2 = (EditText)findViewById(R.id.passReg2);

        regUsuario = (Button)findViewById(R.id.btnRegUsuario);

        // seleccion con el uso de spinner

        /*miSpinner=(Spinner)findViewById(R.id.miSpinner);

        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, state );

        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        miSpinner.setAdapter(adapter_state);

        miSpinner.setOnItemSelectedListener(this);
*/
        // contruccion de mi validador de campos
        validator = new Validator(context);

        validator.setValidationListener(this);

        regUsuario.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                // registrar los datos del usuario en la BD
                String nuevo_nombre = nombreReg.getText().toString();

                String nuevo_pass = passReg.getText().toString();

                //validacion de campos de registro de nuevo usuario

                validator.validate();

                if (ban==1) {
                    Toast.makeText(context, "Usuario registrado en la BD. nombre: " + nuevo_nombre + " pass : " + nuevo_pass, Toast.LENGTH_SHORT).show();
                    // abrir el chat para inciar una conversacion


                }else
                    if (ban==0){
                    Toast.makeText(context, "Llene los datos correctamente.", Toast.LENGTH_SHORT).show();
                }
                // si no muestra nada no se registran los datos en ningun lado
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        ban=1;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error:errors){

            View view = error.getView();

            String message=error.getCollatedErrorMessage(context);

            ban=0;

            if(view instanceof EditText){
                ((EditText) view).setError(message);
            }else {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

   /* @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        miSpinner.setSelection(position);

        String selState = (String)miSpinner.getSelectedItem();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(context,"Seleccione alguna opcion!!!", Toast.LENGTH_SHORT).show();
    }*/
}