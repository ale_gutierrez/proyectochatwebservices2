package com.example.agutierrez.proyectochatwebservices;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;


public class MainActivity extends AppCompatActivity implements ValidationListener {

    @NotEmpty(message = "Escriba su nombre",trim = true)
    EditText nombre;

    //@NotEmpty(message = "Escriba su contraseña")
    @Password(min = 6, message = "Escriba su password")
    EditText password;

    Button btnIngresar;

    Button btnRegistrar;

    Validator validator;

    Context context;

    int ban=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // aqui se defino mis campos de texto de la pantalla principal
        context = this;

        nombre = (EditText)findViewById(R.id.nombre);

        password = (EditText)findViewById(R.id.password);

        btnIngresar = (Button)findViewById(R.id.btnIngresar);

        btnRegistrar = (Button)findViewById(R.id.btnRegistrar);

        validator = new Validator(context);

        validator.setValidationListener(this);

        // ACCIONES DEL BOTON inGRESAR
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // verifcacion de identidad de usuario
                String campo_nombre = nombre.getText().toString();
                String campo_pass = password.getText().toString();

                validator.validate();

                if (campo_nombre.compareTo("ale") == 0 && campo_pass.compareTo("123456") == 0 && ban==1 ) {

                    Toast.makeText(context, "Usted es usuario registrado Bienvenido", Toast.LENGTH_LONG).show();
                    Intent a = new Intent(context, MiChat.class);
                    String[] datos = new String[2];
                    datos[0] = nombre.getText().toString();
                    datos[1] = password.getText().toString();
                    a.putExtra("Datos de Usuario", datos);
                    startActivity(a);
                } else {

                    Toast.makeText(context, "Usted no es usuario regitrado registrese por favor!!!", Toast.LENGTH_LONG).show();
                    // limpiar los datos de el usuario no registrado

                }
            }
        });
        // acciones del boton registrar
        btnRegistrar.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                // procede al registro de usuario

                Intent ru = new Intent(context, RegistroUsuario.class);

                startActivity(ru);
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        ban=1;
        //Toast.makeText(context, "Los datos ingresaron correctamente", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error:errors){
            View view = error.getView();

            String message =error.getCollatedErrorMessage(context);

            ban=0;

            if (view instanceof EditText){
                ((EditText) view).setError(message);
            }else{
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }

        }
    }
}
